import os
from flask import Flask, render_template
from flask import Markup
import markdown

import sys

reload(sys)
sys.setdefaultencoding('utf8')

def make_tree(path):
    tree = dict(name=os.path.basename(path), children=[])
    try: lst = os.listdir(path)
    except OSError:
        pass #ignore errors
    else:
        for name in lst:
            fn = os.path.join(path, name)
            if os.path.isdir(fn):
                tree['children'].append(make_tree(fn))
            else:
                tree['children'].append(dict(name=name))
    return tree

app = Flask(__name__, static_folder='static', static_url_path='')

@app.route('/')
def dirtree():
    path = os.path.expanduser(u'static/subs/')

    index = open('README.md', 'r').read()

    content = Markup(markdown.markdown(index))

    return render_template('index.html', tree=make_tree(path), **locals())

if __name__=="__main__":
    app.run(host='localhost', port=8888, debug=True)
