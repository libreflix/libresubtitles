# [LibreSubtitles](/)

### English `en`
> Repository to create, edit and storage subtitles for *free streaming* productions.

Commom path to each file:

```
libresubtitles/subs/{permalink}/{permalink}_{languageCode}_{countryCode}.vtt
```

More information about WebVTT:

- `code_language`: [https://www.w3schools.com/tags/ref_language_codes.asp](https://www.w3schools.com/tags/ref_language_codes.asp)

- `countryCode`: [https://www.w3schools.com/tags/ref_country_codes.asp](https://www.w3schools.com/tags/ref_country_codes.asp)

Format:

- [WebVTT](https://developer.mozilla.org/en-US/docs/Web/API/WebVTT_API)


#### Contributors:
- [Guilmour Rossi](https://guilmour.org)


To contribute with subtitles, please visit: [https://libregit.org/libreflix/libresubtitles](https://libregit.org/libreflix/libresubtitles)
