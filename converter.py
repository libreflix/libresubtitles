import webvtt
from os import listdir
from os.path import isfile, join




mypath = 'subs/sintel/'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

for subs in onlyfiles:
    webvtt.from_srt(subs).save()
