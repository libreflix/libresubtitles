# LibreSubtitles

### Português `pt-BR`
> Repositório para criar, editar e armazenar legendas para produções audiovisuais no Libreflix.

Caminho para cada arquivo:

```
libresubtitles/subs/{permalink}/{permalink}_{languageCode}_{countryCode}.vtt
```

Mais informações para

- `code_language`: https://www.w3schools.com/tags/ref_language_codes.asp

- `countryCode`: https://www.w3schools.com/tags/ref_country_codes.asp


Mais informações sobre o formato WebVTT:
- [WebVTT](https://developer.mozilla.org/en-US/docs/Web/API/WebVTT_API)

-------------

#### Contributors:
- [Guilmour Rossi](https://guilmour.org)

> Por Libreflix.org
